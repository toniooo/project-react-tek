import { NavigationContainer, useRoute } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { StyleSheet, View, Image, Button } from 'react-native';
import TabMainMenu from './screen/TabMainMenu';
import GithubLogo from './assets/HomeScreen/GithubLogo.png'
import { Provider } from 'react-redux';
import store from './redux/store';

const Stack = createStackNavigator();

function HomeScreen({ navigation }) {
  return (
    <View style={styles.viewMainContainer}>
      <Image source={GithubLogo} style={styles.imageGithubLogo}></Image>
      <Button
        title="Github Search"
        style={styles.buttonNavigationMenu}
        onPress={() => navigation.navigate('TabMenu')}
      />
    </View>
  );
}

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home" screenOptions={{ title: "Github Search Engine", headerTitleAlign: 'center', headerTintColor: '#000', headerTitleStyle: { fontWeight: 'bold' } }} >
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="TabMenu" component={TabMainMenu} navigationOptions={{ header: ({ goBack }) => ({ left: <Left onPress={goBack} /> }) }} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  viewMainContainer: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  imageGithubLogo: {
    flex: 0.6,
    resizeMode: 'contain',
    aspectRatio: 1,
    alignSelf: 'center'
  },
  buttonNavigationMenu: {
    flex: 3
  },
});

export default App;
