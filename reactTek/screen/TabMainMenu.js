import * as React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import TabSearch from "./tabs/TabSearch";
import TabFavorite from "./tabs/TabFavorite";

const Tab = createMaterialTopTabNavigator();

function TabMainMenu() {
    return (
        <Tab.Navigator initialRouteName="Search"
            tabBarOptions={{
                indicatorStyle: {
                    backgroundColor: '#000',
                },
                activeTintColor: '#000',
                labelStyle: { fontSize: 20, fontWeight: 'bold' },
                style: {
                    backgroundColor: 'white',
                    elevation: 0,
                    shadowOpacity: 0,
                    indicatorStyle: {
                        backgroundColor: 'black',
                    }
                },
            }}>
            <Tab.Screen name="Search" component={TabSearch} />
            <Tab.Screen name="Favorite" component={TabFavorite} />
        </Tab.Navigator>
    );
}

export default TabMainMenu;