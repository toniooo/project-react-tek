import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useState, useEffect } from 'react';

function ItemProfile() {
    return (
        <View style={styles.viewMainContainer}>
            <Text>ItemProfile</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    viewMainContainer: {
        backgroundColor: 'black',
        height: 100,
        width: 100,
        flexDirection: 'column',
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
  });

export default ItemProfile;