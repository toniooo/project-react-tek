import * as React from 'react';
import { View, Text, StyleSheet, FlatList, SafeAreaView, StatusBar, Image, TouchableOpacity, Alert, Button } from 'react-native';
import { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { SearchBar } from 'react-native-elements';
import { ToggleButton } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage'
import ImageFolder from '../../../assets/folder.png';

const STORAGE_KEY = '@save_list'


const UnderTabRepo = (DATA) => {
    useEffect(() => {
      readData()
  }, [])
  const [checked, setChecked] = useState(false);
  const [memories, setmemories] = useState([]);
  const [getdata, setgetdata] = useState([]);
  const [currentData, setCurrentData] = useState([]);

  useEffect(() => {
    setCurrentData(DATA.DATA);
  });

  const readData = async () => {
    try {
      const dataFetch = await AsyncStorage.getItem(STORAGE_KEY)

      if (dataFetch !== null) {
        console.log(JSON.parse(dataFetch));
        console.log('DATA READ REPOS');
        setmemories(JSON.parse(dataFetch))
      }
    } catch (e) {
      alert('Failed to fetch the data from storage')
    }
  }

  const saveData = async () => {
    try {
      await AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(memories))
      alert('Data successfully saved')
    } catch (e) {
      alert('Failed to save the data to the storage')
    }
  }

  const renderItem = ({ item }) => (
    // <View style={styles.item}>

      <View style={styles.viewContainerAvatar}>
        <View style={styles.viewContainerHeader}>
          <Image source={ImageFolder} style={styles.imageAvatar}></Image>
        </View>
        <View style={styles.viewContainerRight}>
          <View style={styles.viewContainerUserInfo}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.type}>  ({item.type ? 'Private' : 'Public'})</Text>
          </View>
          <View style={styles.middleLineHeader}>
            <Text style={styles.isForked}>The repository is {item.fork ? '' : 'not'} forked.</Text>
          </View>
          <View style={styles.viewDescription}>
            <Text style={styles.textDescription}>{item.description}</Text>
          </View>
          <View style={styles.lastLineInfo}>
            <Text style={styles.textSize}>Size: {item.size}</Text>
            <Text style={styles.textBranch}>Branch: {item.default_branch}</Text>
          </View>
          <View style={styles.viewContainerUserTouch}>
            <TouchableOpacity style={styles.touchableRepositoryButton}>
              <Text style={styles.touchableButtonStyle}>Contributor</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.touchableFollowerButton}>
              <Text style={styles.touchableButtonStyle}>Issues</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => { console.log(item, 'was add to fav');readData(); setmemories([...memories, item]); saveData(memories); }}>
            <Text>ADD FAV</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.viewContainerFavorite}>

        </View>
      </View>

    // </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={currentData}
        extraData={currentData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'transparent',
  },
  viewContainerAvatar: {
    flexDirection: 'row',
    marginVertical: 10
  },
  viewContainerHeader: {
    flex: 1,
    alignContent: 'flex-start',
    alignSelf: 'flex-start',

  },
  imageAvatar: {
    flex: 1,
    resizeMode: 'contain',
    height: 40,
    width: 40
  },
  name: {
    fontSize: 18,
    alignSelf: 'flex-start',
    fontWeight: 'bold'
  },
  type: {
    fontSize: 16,
    color: 'gray',
    alignSelf: 'flex-end'
  },
  viewContainerRight: {
    padding: 3,
    flex: 4,
    flexDirection: 'column'
  },
  viewContainerUserInfo: {
    flexDirection: 'row'
  },
  middleLineHeader: {
    flex: 1
  },

  isForked: {
    fontSize: 12,
    alignSelf: 'flex-start',
    fontStyle: 'italic'

  },
  viewDescription: {
    flex: 1,
  },
  textDescription: {
    flex: 1,
    fontSize: 14
  },
  lastLineInfo: {
    flex: 1,
    flexDirection: 'row'
  },
  textSize: {
    flex: 1,
    alignSelf: 'flex-start',
    fontSize: 12,
    fontStyle: 'italic'

  },
  textBranch: {
    flex: 1,
    alignSelf: 'flex-start',
    fontSize: 12,
    fontStyle: 'italic'
  },
  viewContainerUserTouch: {
    flexDirection: 'row'
  },
  touchableRepositoryButton: {
    alignSelf: 'flex-start',
    flex: 3
  },
  touchableFollowerButton: {
    alignSelf: 'flex-end',
    flex: 3
  },
  touchableButtonStyle: {
    fontSize: 15,
    color: 'blue'
  },
  viewContainerFavorite: {
    flex: 1,

    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  actionButtonIcon: {
    flex: 1,
    height: '100%',
    width: '100%',
    alignSelf: 'center'

  }
});

export default UnderTabRepo;