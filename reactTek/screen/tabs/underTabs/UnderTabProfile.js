import * as React from 'react';
import { View, Text, StyleSheet, FlatList, SafeAreaView, StatusBar, Image, TouchableOpacity } from 'react-native';
import { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { SearchBar } from 'react-native-elements';
import { ToggleButton } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage'


const UnderTabProfile = (DATA) => {
     useEffect(() => {
      readDataUsers()
     }, [])
    const [checked, setChecked] = useState(false);
    const [currentData, setCurrentData] = useState([]);
    const [memoriesUsers, setmemoriesUsers] = useState([]);
    const [getdataUsers, setgetdataUsers] = useState([]);
    const STORAGE_USERS = '@users'

    const readDataUsers = async () => {
        try {
          const dataFetchUser = await AsyncStorage.getItem(STORAGE_USERS)
    
          if (dataFetchUser !== null) {
            console.log(JSON.parse(dataFetchUser));
            console.log('DATA READ USERS');
            setmemoriesUsers(JSON.parse(dataFetchUser))
          }
        } catch (e) {
          alert('Failed to fetch the data from storage')
        }
      }

    const saveDataUsers = async () => {
        try {
            await AsyncStorage.setItem(STORAGE_USERS, JSON.stringify(memoriesUsers))
            alert('Data successfully saved')
        } catch (e) {
            alert('Failed to save the data to the storage')
        }
    }

    useEffect(() => {
        setCurrentData(DATA.DATA);
    });
    const renderItem = ({ item }) => (
        <View style={styles.viewContainerAvatar}>
            <View style={styles.viewContainerHeader}>
                <Image source={{
                    uri: item.avatar_url,
                }} style={styles.imageAvatar}></Image>
            </View>
            <View style={styles.viewContainerRight}>
                <View style={styles.viewContainerUserInfo}>
                    <Text style={styles.login}>{item.login}</Text>
                    <Text style={styles.type}>  ({item.type})</Text>
                </View>
                <View style={styles.viewContainerUserTouch}>
                    <TouchableOpacity style={styles.touchableRepositoryButton}>
                        <Text style={styles.touchableButtonStyle}>Repository</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchableFollowerButton}>
                        <Text style={styles.touchableButtonStyle}>Followers</Text>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={styles.viewContainerFavorite}>
                <TouchableOpacity onPress={() => { console.log(item, 'was add to fav');readDataUsers(); setmemoriesUsers([...memoriesUsers, item]); saveDataUsers(memoriesUsers) }} style={{ alignSelf: 'center', justifyContent: 'center' }}>
                    <Text style={{ alignSelf: 'center', fontSize: 16 }}>ADD FAV</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={currentData}
                extraData={currentData}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
    },
    item: {
        backgroundColor: 'transparent',
    },
    viewContainerAvatar: {
        flexDirection: 'row',
        marginVertical: 10
    },
    viewContainerHeader: {
        flex: 1,
        alignContent: 'flex-start',
        alignSelf: 'flex-start'

    },
    imageAvatar: {
        flex: 1,
        resizeMode: 'contain'
    },
    login: {
        fontSize: 24,
        alignSelf: 'flex-start',
        fontWeight: 'bold'
    },
    type: {
        fontSize: 20,
        color: 'gray',
        alignSelf: 'flex-end'
    },
    viewContainerRight: {
        padding: 3,
        marginLeft: 10,
        flex: 4,
        flexDirection: 'column'
    },
    viewContainerUserInfo: {
        flexDirection: 'row'
    },
    viewContainerUserTouch: {
        flexDirection: 'row'
    },
    touchableRepositoryButton: {
        alignSelf: 'flex-start',
        flex: 3
    },
    touchableFollowerButton: {
        alignSelf: 'flex-end',
        flex: 3
    },
    touchableButtonStyle: {
        fontSize: 18,
        color: 'blue'
    },
    viewContainerFavorite: {
        flex: 1,
        marginTop: 12,
        alignContent: 'center',
        alignItems: 'center'
    },
    actionButtonIcon: {
        flex: 1,
        height: '100%',
        width: '100%'

    }
});

export default UnderTabProfile;