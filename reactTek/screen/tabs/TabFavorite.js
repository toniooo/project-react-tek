import * as React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { useState, useEffect } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import UnderTabProfile from './underTabs/UnderTabProfile';
import UnderTabRepo from './underTabs/UnderTabRepo';
import AsyncStorage from '@react-native-community/async-storage'

function TabFavorite() {
    useEffect(() => {
        readData()
     }, [])
     useEffect(() => {
      readDataUsers()
     }, [])

    const Tab = createMaterialTopTabNavigator();
    const [getdata, setgetdata] = useState([]);
    const [getdataUser, setgetdataUser] = useState([]);
    const STORAGE_KEY = '@save_list'
    const STORAGE_USERS = '@users'
    
      const readData = async () => {
        try {
          const dataFetch = await AsyncStorage.getItem(STORAGE_KEY)
    
          if (dataFetch !== null) {
            console.log(JSON.parse(dataFetch));
            console.log('DATA READ REPOS');
            setgetdata(JSON.parse(dataFetch))
          }
        } catch (e) {
          alert('Failed to fetch the data from storage')
        }
      }
      const readDataUsers = async () => {
        try {
          const dataFetchUser = await AsyncStorage.getItem(STORAGE_USERS)
    
          if (dataFetchUser !== null) {
            console.log(JSON.parse(dataFetchUser));
            console.log('DATA READ USERS');
            setgetdataUser(JSON.parse(dataFetchUser))
          }
        } catch (e) {
          alert('Failed to fetch the data from storage')
        }
      }

      const clearStorage = async () => {
        try {
          await AsyncStorage.clear()
          alert('Storage successfully cleared!')
        } catch (e) {
          alert('Failed to clear the async storage.')
        }
      }

    return (
        <View style={styles.viewMainContainer}>
            <View style={styles.viewSearbarContainer}>
            <Tab.Navigator initialRouteName="Profile"
                tabBarOptions={tabBarOptions}>
                <Tab.Screen name="Profile" children={() => <UnderTabProfile DATA={getdataUser}/>} />
                <Tab.Screen name="Repository" children={() => <UnderTabRepo DATA={getdata}/>} />
            </Tab.Navigator>
            </View>
            <TouchableOpacity onPress={() => { clearStorage() }}>
                <Text>CLEAR FAV</Text>
            </TouchableOpacity>
        </View>
    );
}

const tabBarOptions = {
    indicatorStyle: {
        backgroundColor: '#000',
    },
    activeTintColor: '#000',
    labelStyle: { fontSize: 14, fontWeight: 'bold' },
    swipeEnabled: false,
    lazyLoad: true,
    animationEnabled: false,
    style: {

        backgroundColor: 'white',
        borderRadius: 5,
        indicatorStyle: {
            backgroundColor: 'black',
        },
        justifyContent: 'center',
        alignContent: 'center'
    },
}

const styles = StyleSheet.create({
    viewMainContainer: {
        flexDirection: 'column',
        marginTop: 15,
        flex: 1,
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    viewSearbarContainer: {

        backgroundColor: 'transparent',
        flexDirection: 'row',
        flex: 2,
        alignSelf: 'flex-start',
        paddingRight: 20,
        paddingLeft: 20
    }
});

export default TabFavorite;