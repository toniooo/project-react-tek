import * as React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { useState, useEffect } from 'react';
import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import UnderTabProfile from './underTabs/UnderTabProfile';
import UnderTabRepo from './underTabs/UnderTabRepo';
import { fetchUsers } from '../../redux/services/index';
import { fetchRepos } from '../../redux/services/index';
import { getUsersFromUsername } from '../../redux/actions/userActions';




function TabSearch() {

    const [search, setSearch] = useState('');
    const [arrayholder, setArrayHolder] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [dataSource, setDataSource] = useState([]);
    const [dataSourceRepo, setDataSourceRepo] = useState([]);

    const Tab = createMaterialTopTabNavigator();

    useEffect(() => {
        let mounted = true;

        if (mounted) {
            setIsLoading(false)
            // setArrayHolder(dataFile),
        }
        mounted = false;
    }, [])

    const clear = () => {
        setSearch('');
    };

    if (isLoading) {
        return (
            <View style={{ flex: 1, paddingTop: 20 }}>
                <ActivityIndicator />
            </View>
        );
    }

    const SearchFilterFunction = (text) => {

        fetchUsers(text).then(data => setDataSource(data.items));
        fetchRepos(text).then(data => setDataSourceRepo(data.items));
        setSearch(text);
    }

    return (
        <View style={styles.viewMainContainer}>
            <SearchBar round inputContainerStyle={{ backgroundColor: '#e2e8e9' }}
                searchIcon={{ size: 24 }}
                containerStyle={styles.searchbarContainer}
                onChangeText={text => SearchFilterFunction(text)}
                onClear={text => SearchFilterFunction('')}
                placeholder="Search github profile or repository..."
                value={search} />
            <View style={styles.viewTabNavigator}>
                <Tab.Navigator initialRouteName="Profile"
                    tabBarOptions={tabBarOptions}>
                    <Tab.Screen name="Profile" children={() => <UnderTabProfile DATA={dataSource}/>} />
                    <Tab.Screen name="Repository" children={() => <UnderTabRepo DATA={dataSourceRepo}/>} />
                </Tab.Navigator>
            </View>
        </View>
    );
}

const tabBarOptions = {
    indicatorStyle: {
        backgroundColor: '#000',
    },
    activeTintColor: '#000',
    labelStyle: { fontSize: 14, fontWeight: 'bold' },
    swipeEnabled: false,
    lazyLoad: true,
    animationEnabled: false,
    style: {

        backgroundColor: 'white',
        borderRadius: 5,
        indicatorStyle: {
            backgroundColor: 'black',
        },
        justifyContent: 'center',
        alignContent: 'center'
    },
}

const styles = StyleSheet.create({
    viewMainContainer: {
        flex: 1,
        flexDirection: 'column',
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    viewTabNavigator: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        flex: 2,
        alignSelf: 'flex-start',
        paddingRight: 20,
        paddingLeft: 20
    },
    // viewSearchBar: {
    //     flex: 1,
    //     alignSelf: 'flex-start',
    //     backgroundColor: 'white',
    // },
    searchbarContainer: {
        backgroundColor: 'transparent',
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent',
    },
    scrollviewResponse: {
        flex: 1
    },
});

export default TabSearch;