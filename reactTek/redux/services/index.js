import { Octokit } from "@octokit/rest"

const octokit = new Octokit({
  auth: '0c020b5d123b186684d70d6af84a12dee79a25c9'
})

const parseUsersInfo = (users) => {
  return users.map(user => ({
    login: user.login,
    type: user.type,
    avatar_url: user.avatar_url
  }))
}

const parseReposInfo = (repos) => {
  return repos.map(repo => ({
    name: repo.name,
    private: repo.private,
    owner: repo.owner.login,
    fork: repo.fork,
    description: repo.description,
    default_branch: repo.default_branch
  }))
}

const parseIssuesInfo = (issues) => {
  return issues.map(issue => ({
    title: issue.title,
    state: issue.state,
    body: issue.body,
    user: issue.user.login
  }))
}

export const fetchUsers = (username) => {
  return octokit.search.users({
    q: username
  })
    .then(res => res.data)
    .catch(e => {
      console.error(e)
      return new Error({ title: "error fetch repositories", msg: e })
    });
}

export const fetchRepos = (repoName) => {
  return octokit.search.repos({
    q: repoName
  })
    .then(res => res.data)
    .catch(e => {
      console.error(e)
      return new Error({ title: "error fetch repositories", msg: e })
    });
}

export const getReposFromUser = (username) => {
  return octokit.repos.listForUser({
    username
  }).then(res => res.data)
    .then(parseReposInfo)
    .catch(err => {
      console.error(err)
      throw new Error(err)
    })
}

export const getFollowersFromUser = (username) => {
  return octokit.users.listFollowersForUser({
    username
  }).then(res => res.data)
    .then(parseUsersInfo)
    .catch(err => {
      console.error(err)
      throw new Error(err)
    })
}

export const getContributorsFromRepo = ({ owner, name }) => {
  return octokit.repos.listContributors({
    owner,
    repo: name
  }).then(res => res.data)
    .then(parseUsersInfo)
    .catch(err => {
      console.error(err)
      throw new Error(err)
    })
}

export const getIssuesFromRepo = ({ owner, name }) => {
  return octokit.issues.listForRepo({
    owner,
    repo: name
  }).then(res => res.data)
    .then(parseIssuesInfo)
    .catch(err => {
      console.error(err)
      throw new Error(err)
    })
}
