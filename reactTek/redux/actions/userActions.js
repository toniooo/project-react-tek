import { fetchUsers, getFollowersFromUser, getReposFromUser } from '../services/index'
import { uConstants } from '../reducers/userReducer'

const usersRequest = () => ({
    type: uConstants.USERS_LIST_REQUEST
})

const usersSuccess = (data) => ({
    type: uConstants.USERS_LIST_SUCCESS,
    data
})

const usersFailure = (error) => ({
    type: uConstants.USERS_LIST_FAILURE,
    error
})

export const getUsersFromUsername = (username) => (dispatch) => {
    dispatch(usersRequest)
    fetchUsers(username)
        .then(data => dispatch(usersSuccess(data)))
        .catch(error => dispatch(usersFailure(error)))
}

export const reposFromUser = username => dispatch => {
    dispatch({ type: uConstants.USER_REPOS_REQUEST })
    getReposFromUser(username)
        .then(data => dispatch({ type: uConstants.USER_REPOS_SUCCESS, data }))
        .catch(error => dispatch({ type: uConstants.USER_REPOS_FAILURE, error }))
}

export const followersFromUser = (username) => (dispatch) => {
    dispatch({ type: uConstants.USER_FOLLOWERS_REQUEST })
    getFollowersFromUser(username)
        .then(data => dispatch({ type: uConstants.USER_FOLLOWERS_SUCCESS, data }))
        .catch(error => dispatch({ type: uConstants.USER_FOLLOWERS_FAILURE, error }))
}