import { fetchRepos, getContributorsFromRepo, getIssuesFromRepo } from '../services/index'
import { rConstants } from '../reducers/repoReducer'

const reposRequest = () => ({
    type: rConstants.REPOS_LIST_REQUEST
})

const reposSuccess = (data) => ({
    type: rConstants.REPOS_LIST_SUCCESS,
    data
})

const reposFailure = (error) => ({
    type: rConstants.REPOS_LIST_FAILURE,
    error
})

export const getRepos = (repoName) => (dispatch) => {
    dispatch(reposRequest)
    fetchRepos(repoName)
        .then(data => dispatch(reposSuccess(data)))
        .catch(error => dispatch(reposFailure(error)))
}

export const contributorsFromRepo = (repo) => (dispatch) => {
    dispatch({ type: rConstants.REPO_CONTRIBUTORS_REQUEST})
    getContributorsFromRepo(repo)
        .then(data => dispatch({ type: rConstants.REPO_CONTRIBUTORS_SUCCESS, data }))
        .catch(error => dispatch({ type: rConstants.REPO_CONTRIBUTORS_FAILURE, error }))
}

export const contributorsFromRepo = (repo) => (dispatch) => {
    dispatch({ type: rConstants.REPO_ISSUES_REQUEST})
    getIssuesFromRepo(repo)
        .then(data => dispatch({ type: rConstants.REPO_ISSUES_SUCCESS, data }))
        .catch(error => dispatch({ type: rConstants.REPO_ISSUES_FAILURE, error }))
}