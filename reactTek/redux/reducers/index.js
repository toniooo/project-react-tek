import { combineReducers } from 'redux'
import { repoReducer } from './repoReducer'
import { userReducer } from './userReducer'

const appReducer = combineReducers({
    repo: repoReducer,
    user: userReducer
})

// const rootReducer = (state, action) => {
//     if (action.type === userConstants.LOGOUT)
//         state = undefined
//     return appReducer(state, action)
// }

export default appReducer