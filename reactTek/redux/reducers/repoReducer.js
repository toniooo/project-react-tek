const initialState = {
}

export const rConstants = {
    REPOS_LIST_REQUEST: 'REPOS_LIST_REQUEST',
    REPOS_LIST_SUCCESS: 'REPOS_LIST_SUCCESS',
    REPOS_LIST_FAILURE: 'REPOS_LIST_FAILURE',
    REPO_CONTRIBUTORS_REQUEST: 'REPO_CONTRIBUTORS_REQUEST',
    REPO_CONTRIBUTORS_SUCCESS: 'REPO_CONTRIBUTORS_SUCCESS',
    REPO_CONTRIBUTORS_FAILURE: 'REPO_CONTRIBUTORS_FAILURE',
    REPO_ISSUES_REQUEST: 'REPO_ISSUES_REQUEST',
    REPO_ISSUES_SUCCESS: 'REPO_ISSUES_SUCCESS',
    REPO_ISSUES_FAILURE: 'REPO_ISSUES_FAILURE',
};

export const repoReducer = (state = initialState, action) => {
    switch (action.type) {
        case rConstants.REPOS_LIST_REQUEST:
            return {
                requesting: true
            }
        case rConstants.REPOS_LIST_SUCCESS:
            return {
                repos: action.data,
            }
        case rConstants.REPOS_LIST_FAILURE:
            return {
                error: action.error,
            }
        case rConstants.REPO_CONTRIBUTORS_REQUEST:
            return {
                ...state,
                requesting: true
            }
        case rConstants.REPO_CONTRIBUTORS_SUCCESS:
            return {
                ...state,
                requesting: false,
                contributors: action.data,
            }
        case rConstants.REPO_CONTRIBUTORS_FAILURE:
            return {
                error: action.error,
            }
        case rConstants.REPO_ISSUES_REQUEST:
            return {
                ...state,
                requesting: true
            }
        case rConstants.REPO_ISSUES_SUCCESS:
            return {
                ...state,
                requesting: false,
                issues: action.data,
            }
        case rConstants.REPO_ISSUES_FAILURE:
            return {
                error: action.error,
            }
        default:
            return state
    }
}