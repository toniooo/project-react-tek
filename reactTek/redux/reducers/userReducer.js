const initialState = {
}

export const uConstants = {
    USERS_LIST_REQUEST: 'USERS_LIST_REQUEST',
    USERS_LIST_SUCCESS: 'USERS_LIST_SUCCESS',
    USERS_LIST_FAILURE: 'USERS_LIST_FAILURE',
    USER_FOLLOWERS_REQUEST: 'USER_FOLLOWERS_REQUEST',
    USER_FOLLOWERS_SUCCESS: 'USER_FOLLOWERS_SUCCESS',
    USER_FOLLOWERS_FAILURE: 'USER_FOLLOWERS_FAILURE',
    USER_REPOS_REQUEST: 'USER_REPOS_REQUEST',
    USER_REPOS_SUCCESS: 'USER_REPOS_SUCCESS',
    USER_REPOS_FAILURE: 'USER_REPOS_FAILURE',
};

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case uConstants.USERS_LIST_REQUEST:
            return {
                requesting: true
            }
        case uConstants.USERS_LIST_SUCCESS:
            return {
                users: action.data,
            }
        case uConstants.USERS_LIST_FAILURE:
            return {
                error: action.error,
            }
        case uConstants.USER_FOLLOWERS_REQUEST:
            return {
                ...state,
                requesting: true
            }
        case uConstants.USER_FOLLOWERS_SUCCESS:
            return {
                ...state,
                requesting: false,
                followers: action.data,
            }
        case uConstants.USER_FOLLOWERS_FAILURE:
            return {
                error: action.error,
            }
        case uConstants.USER_REPOS_REQUEST:
            return {
                ...state,
                requesting: true
            }
        case uConstants.USER_REPOS_SUCCESS:
            return {
                ...state,
                requesting: false,
                repos: action.data,
            }
        case uConstants.USER_REPOS_FAILURE:
            return {
                error: action.error,
            }
        default:
            return state
    }
}